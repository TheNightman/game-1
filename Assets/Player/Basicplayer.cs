using UnityEngine;
using System.Collections;

public class Basicplayer : MonoBehaviour
{

		Vector3 startingPosition;

		public float speed;
		public float jspeed;
		//public float jump;
		public float dspeed;
		public bool grounded = true;
		public int direction = 0;
	
		void Start ()
		{
				//speed = 0.1f;
				startingPosition = transform.position;
		}

	

		void Update ()
		{
		
	
				if (direction == 1) {
						transform.Translate (Vector3.right * speed * Time.deltaTime);
				}
				if (direction == 2) {
						transform.Translate (Vector3.left * speed * Time.deltaTime);
				}
				if (Input.GetButton ("Jump")) {
		
						Jump ();
			
				}
		
		
		}
	
		void Jump ()
		{
		
				if (grounded == true) {
			
			
						transform.GetComponent<Rigidbody> ().AddForce (Vector3.up * jspeed * Time.deltaTime, ForceMode.VelocityChange);	
			
						grounded = false;
			
				}
	
		
		}

		void OnCollisionEnter (Collision col)
		{
				if (col.gameObject.name == "platfrom") {
						grounded = true;
						//check message upon collition for functionality working of code.
						//Debug.Log ("I am colliding with something");	
				}
		}


		public void go ()
		{
				direction = 1;
		}
		
		public void Reset ()
		{
				transform.position = startingPosition;
				direction = 0;
		}
}