    // simple x,y,z movement over a distance
    var duration = 1.0;
    var is2D = true;
    var posEndDistance : Vector3;
    private var posStart : Vector3;
    private var posEnd : Vector3;
     
    function Start () {
    
      posStart = transform.position;
      if (is2D) { posEndDistance.z = 0.0; } // force no movement on the "z" axis
      // I could have used Vector3 to assign, but this looks more readable to me
      posEnd.x = transform.position.x + posEndDistance.x;
      posEnd.y = transform.position.y + posEndDistance.y;
      posEnd.z = transform.position.z + posEndDistance.z;
    }
     
    function Update () {
        var lerp = Mathf.PingPong (Time.time, duration) / duration;
        transform.position = Vector3.Lerp (posStart, posEnd, lerp);
    }