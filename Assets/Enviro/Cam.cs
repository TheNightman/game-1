using UnityEngine;
using System.Collections;

public class Cam : MonoBehaviour
{

		// Use this for initialization
		public float dampTime = 0.15f;
		private Vector3 velocity = Vector3.zero;
		public Transform target;
		public GameObject player;
	private bool start = false;
 
		void Awake ()
		{


		}
	public void Start (){


		start = true;


	}

		void Update ()
		{
				
						Vector3 point = GetComponent<Camera> ().WorldToViewportPoint (target.position);
						Vector3 delta = target.position - GetComponent<Camera> ().ViewportToWorldPoint (new Vector3 (0.3f, 0.3f, point.z)); //(new Vector3(0.5, 0.5, point.z));
						Vector3 destination = transform.position + delta;
						transform.position = Vector3.SmoothDamp (transform.position, destination, ref velocity, dampTime);
		
				
		}
}