﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelStartButton : MonoBehaviour
{

		Playstate gameState = Playstate.Stop;
		Basicplayer player;
		Text buttonLabel;

		// Use this for initialization
		void Start ()
		{
				player = GameObject.Find ("player").GetComponent<Basicplayer> ();
				buttonLabel = transform.FindChild ("Button").FindChild ("Text").GetComponent<Text> ();
				buttonLabel.text = "GO!";
		}
	
		public void ButtonClicked ()
		{
				if (gameState == Playstate.Stop) {
						gameState = Playstate.normal;
						player.go ();
						buttonLabel.text = "RESET";
				} else if (gameState == Playstate.normal) {
						gameState = Playstate.Stop;
						player.Reset ();
						buttonLabel.text = "GO!";
				}
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}
}
