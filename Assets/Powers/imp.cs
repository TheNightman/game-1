using UnityEngine;
using System.Collections;

public class imp : MonoBehaviour
{

	public float force;
	public float area;
	public float radius;
	public Color gizmoColor = Color.red;
	public float load;
	public Rigidbody pre;

	public int points;

	void Awake ()
	{
		points = 0;
		
	}

	void Update ()
	{
	
		Vector3 exolosionPos = transform.position;
		Collider[] colliders = Physics.OverlapSphere (exolosionPos, radius); 
	
		foreach (Collider c in colliders) {
			if (!c) {
			}
				
			if (c.GetComponent<Rigidbody>() && c.gameObject.tag == "Player") {
				c.GetComponent<Rigidbody>().AddExplosionForce (force * Time.deltaTime, exolosionPos, area, 0, ForceMode.Force);
				
			}
			
		}
		
		
		
		
		if(load == 2){
			
			Vector3 wordPos;
			wordPos = gameObject.transform.position;
			
			Instantiate (pre, wordPos, Quaternion.identity);
			//Debug.Log("im here friend");
			
			load += 1;
			
			force = 0;
		}
		
			
			
		
	}
	
	void OnCollisionEnter (Collision col)
	{
		
		
		if (col.gameObject.tag == "Player" && load < 2 && points == 2) {
			Destroy (col.gameObject);
			load += 1;
		//	Debug.Log (load);
			
		}
		
		
		
			
			
			
		
	}

	void OnDrawGizmos ()
	{
 
		Gizmos.color = gizmoColor;
 
		Gizmos.DrawWireSphere (transform.position, radius);
		//	Gizmos.DrawWireSphere(transform.position, area);
 
	}
}