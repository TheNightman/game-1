using UnityEngine;
using System.Collections;

public class GrabHandle : MonoBehaviour
{
		private Vector3 screenPoint;
		public GameObject player;
		private Basicplayer direct;
		private bool hold = true;

		void awake ()
		{
		
		
				direct = player.GetComponent<Basicplayer> ();		
		
		
		
		}

		public void Hold ()
		{
		
		hold = ! hold;
		
		}
	
		void OnMouseDown ()
		{

				if (hold == true) {
						screenPoint = Camera.main.WorldToScreenPoint (gameObject.transform.position);
				}
		}

		void OnMouseDrag ()
		{
				if (hold == true) {
						Vector3 currentScreenPoint = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
						Vector3 currentPos = Camera.main.ScreenToWorldPoint (currentScreenPoint);
						transform.position = currentPos;
				}
			
		}
	
}
	
